module NatBool where

module NatBool where

open import lib

-- 2.1

data Tm         : Set where
  num           : ℕ → Tm
  _+Tm_         : (t t' : Tm) → Tm
  isZero        : (t : Tm) → Tm
  true          : Tm
  false         : Tm
  if_then_else_ : (t t' t'' : Tm) → Tm

height : Tm → ℕ
height (num n) = 0
height (t +Tm t') = 1 + max (height t) (height t')
height (isZero t) = 1 + height t
height true = 0
height false = 0
height (if t then t' else t'') = 1 + max (height t) (max (height t') (height t''))

trues : Tm → ℕ
trues (num n) = 0
trues (t +Tm t') = trues t + trues t'
trues (isZero t) = trues t
trues true = 1
trues false = 0
trues (if t then t' else t'') = trues t + trues t' + trues t''

lemma : (t : Tm) → trues t ≤ 3 ^ height t
lemma (num n) = z≤n
lemma (t +Tm t') =
  ≤-trans {trues t + trues t'}
                (+≤+ (lemma t) (lemma t'))
                (subst ((3 ^ height t) + (3 ^ height t') ≤_) (+-assoc x x (x + 0)) almost)
  where
    x = 3 ^ max (height t) (height t')
    h = max≤ (height t) (height t')
    almost = ≤mon+ (+≤+ (≤mon^ (proj₁ h)) (≤mon^ (proj₂ h)))
lemma (isZero t) = ≤mon+ (lemma t)
lemma true = s≤s z≤n
lemma false = z≤n
lemma (if t then t' else t'') =
  ≤-trans {trues t + trues t' + trues t''}
                (+≤+ (+≤+ (lemma t) (lemma t')) (lemma t''))
                (subst ((3 ^ height t) + (3 ^ height t') + (3 ^ height t'') ≤_) ≡lemma almost)
  where
    x = 3 ^ max (height t) (max (height t') (height t''))
    h = max≤' (height t) (height t') (height t'')
    h₁ = proj₁ h
    h₂ = proj₁ (proj₂ h)
    h₃ = proj₂ (proj₂ h)
    almost : (3 ^ height t  + 3 ^ height t' + 3 ^ height t'' ≤ x + x + x)
    almost = +≤+ (+≤+ (≤mon^ h₁) (≤mon^ h₂)) (≤mon^ h₃)
    ≡lemma : x + x + x ≡ x + (x + (x + 0))
    ≡lemma = subst (λ y → x + x + y ≡ x + (x + (x + 0))) (+-comm x 0) (+-assoc x x (x + 0))
